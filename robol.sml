datatype direction = North | South | East | West;

datatype exp =
	Ident of string
| Number of  int
| ArithExp of aExp
| BooleanExp of bExp
and aExp = 
	Plus of exp*exp
| Minus of exp*exp
| Multi of exp*exp
and bExp = 
LargerThen of exp*exp
| LessThen of exp*exp
| Equal of exp*exp;

datatype stmt =
	Stop
| Move of direction * exp
| Assignment of string *exp
| While of bExp*stmt list;
 
type position = int*int;
datatype start = Start of position
type vardec = string * exp;
type robot = vardec list *start;
type grid = int*int;
type program = grid *robot;

exception OutOfBounds of position*direction;
exception NotAVar of string;

fun getDec(s,nil) = raise NotAVar s
| getDec(s1,(s2,exp2)::declist) =  if s1 = s2 then exp2
else getDec(s,declist);  

fun assignDec((s,_),nil) = raise NotAVar s
| assignDec((s1,exp1),(s2,exp2)::declist) = if s1 = s2 then [(s1,exp2)]@declist 
else [(s2,exp2)]@assignDec((s1,s2),declist);

fun evalExp(LargerThan(e1,e2),decls) = 
if evalExp(e1,decls) > evalExp(e2,decls) then 1 else 0
| evalExp(LessThan(e1,e2),decls) = 
if evalExp(e1,decls) < evalExp(e2,decls) then 1 else 0
| evalExp(Equal(e1,e2),decls) = 
if evalExp(e1,decls) = evalExp(e2,decls) then 1 else 0
| evalExp(Plus(e1,e2)) = evalExp(e1) + evalExp(e2)
| evalExp(Minus(e1,e2)) = evalExp(e1) - evalExp(e2)
| evalExp(Mult(e1,e2)) = evalExp(e1) * evalExp(e2)
| evalExp(Numner(i)) = i
| evalExp(Ident(s),decls) = evalExp(getDec(s,decls),declist);


fun move(dir,ex1)

fun interpret(((_,_),(_,Start(xpos,ypos),nil))) = (xpos,ypos)
| interpret(((x,y),(declist,Start(xpos,ypos),Move(dir)::stmlist))) = 
| interpret(((x,y),(declist,Start(xpos,ypos),Assignment(s,e)::stmlist))) = 
| interpret(((x,y),(declist,Start(xpos,ypos),While(bexp,stml)::stmlist))) = 
if evalExp(bexp,declist) = 1 then 
 interpret(((x,y),(declist,Start(xpos,ypos),stml@[While(bexp,stml)]@stmlist))) 
else interpret(((x,y),(declist,Start(xpos,ypos),stmlist))); 



